import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ForcaJogador {
    public static void main(String[] args) throws IOException {
        String hostName = "localhost";
        int portNumber = 1024; //Porta - MESTRE.
        int portNumber2 = 1025; //Porta - JOGADOR.

        System.out.println("Jogador iniciado.");
        try {
            //Recebe - CLIENTE
            Socket echoSocket2 = new Socket(hostName, portNumber);
            BufferedReader in = new BufferedReader(new InputStreamReader(echoSocket2.getInputStream()));
            BufferedReader in2 = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Conectado ao Mestre.");

            //Envia - SERVIDOR
            ServerSocket serverSocket = new ServerSocket(portNumber2);
            Socket clientSocket = serverSocket.accept();
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            /////

            String userInput;
            char op = 's';
            do{
                String linha = in.readLine();
                if(linha.charAt(0) == '1'){
                    String vet[] = linha.split(":");
                    System.out.println("A palavra era " + vet[1] + ". Parabens, você acertou. :D\n\n\n");
                    userInput = " ";
                } else if(linha.charAt(0) == '2'){
                    String vet[] = linha.split(":");
                    System.out.println("A palavra era " + vet[1] + ". Você errou, tente novamente. :(\n\n\n");
                    userInput = " ";
                } else {
                    System.out.println(linha);
                    userInput = in2.readLine();
                    out.println(userInput);
                    System.out.println("\n");
                }
            } while (true);

        } catch (IOException e) {
            System.out.println("Erro detectado ao tentar ouvir a porta " + portNumber + " ou escutar a conexão.");
            System.out.println(e.getMessage());
        }
    }
}
