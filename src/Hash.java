import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Hash {
    private Object[] vet;
    final int tam = 50;
    private final String caminho = "./palavras.txt";

    public Hash() {
        this.vet = new Object[this.tam];
        for (int i = 0; i < vet.length; i++) {
            vet[i] = new ArrayList <String> ();
        }
    }
    public void carregaHashList(){
        FileReader arquivo;
        BufferedReader leitor;
        try{
            arquivo = new FileReader(this.caminho);
            leitor = new BufferedReader(arquivo);
            while(leitor.ready()){
                String s = leitor.readLine();
                this.insere(s);
            }
            leitor.close();
            arquivo.close();
            System.out.println("Lista Carregado com sucesso!");
        }catch(IOException e){
            System.out.println("Erro na abertura do arquivo!");
        }
    }

    void insere(String s){
        int i = dispersao(s);
        ((ArrayList <String>)this.vet[i]).add(s);
    }

    static int dispersao(String s){
        return s.length();
    }

    public String escolhePalavra(){
        String palavra = "";
        int min = 2, max = 15, min2, max2;
        int rand, rand2 = 0;

        do {
            rand = ((int)(Math.random()*(max-min)))+min;
        } while (((ArrayList <String>)this.vet[rand]).isEmpty());
        do{
            max2 = ((ArrayList <String>)this.vet[rand]).size();
            min2 = 0;
            rand2 = ((int)(Math.random()*(max2-min2)))+min2;
        } while (((ArrayList <String>)this.vet[rand]).get(rand2) == null);
        palavra = ((ArrayList <String>)this.vet[rand]).get(rand2);
        return palavra.toLowerCase();
    }

    public String toString(){
        String saida = "";
        for (int i = 0; i < this.vet.length; i++) {
            ArrayList <String> array = (ArrayList <String>)this.vet[i];
            saida += array.toString();
            saida += "\n";
        }
        return saida;
    }
}
