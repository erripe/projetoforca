import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class ForcaMestre {
    public static void main(String[] args) throws IOException {
        Hash dici = new Hash();
        dici.carregaHashList();
        //System.out.println(dici.toString());

        String hostName = "localhost";
        int portNumber = 1024; //Porta - MESTRE.
        int portNumber2 = 1025; //Porta - JOGADOR.
        System.out.println("Mestre Iniciado.");
        try {
            //Envia - SERVIDOR
            ServerSocket serverSocket = new ServerSocket(portNumber);
            Socket clientSocket = serverSocket.accept();
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);

            //Recebe - CLIENTE
            Socket echoSocket = new Socket(hostName, portNumber2);
            BufferedReader in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
            System.out.println("Conectado ao Jogador.");
            /////


            //Jogo
            do {
                boolean won = false;
                int vida = 8;
                String palavra = dici.escolhePalavra();
                System.out.println("Palavra: " + palavra);
                char c, aux[] = converteStringVetChar(palavra);
                //Jogo iniciado;
                String linha;

                while (!won && vida != 0) {
                    out.println("Vidas: " + vida + " Palavra: " + printCharVet(aux)); //Envia o vet aux com as palavras em branco.
                    linha = in.readLine();
                    System.out.println("Tentativa: " + linha);
                    if(!linha.isEmpty()){
                        c = linha.charAt(0); //Recebe resposta.
                    } else {
                        c = ' ';
                    }
                    if(!preencheAux(palavra, aux, c)){
                        vida--;
                    }
                    if (palavra.equalsIgnoreCase(converteCharVet(aux))) {
                        won = true;
                        out.println("1:"+palavra);
                    }
                }
                if(vida == 0){
                    out.println("2:"+palavra);
                }
            } while (true);
        } catch (UnknownHostException e) {
            System.err.println("Ocorreu um erro ao tentar conectar ao servidor " + hostName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Não foi possível conectar ao Servidor " + hostName);
            e.printStackTrace();
            System.exit(1);
        }
    }

    public static char[] converteStringVetChar(String s){
        char aux[] = new char [s.length()];
        for (int i = 0; i < aux.length; i++) {
            if(s.charAt(i) == '-'){
                aux[i] += '-';
            } else {
                aux[i] += '_';
            }
        }
        return aux;
    }

    public static boolean preencheAux(String resposta, char[] aux, char c){
        boolean tem = false;
        if(c == 'a' || c == 'e' ||c == 'i' ||c == 'o' ||c == 'u'){
            char vet[] = null;
            if (c == 'a'){
                char v[] = {'a','á','â','ã'};
                vet = v;
            } else if (c == 'e'){
                char[] v = {'e','é','ê'};
                vet = v;
            } else if (c == 'i'){
                char[] v = {'i','í','î'};
                vet = v;
            } else if (c == 'o'){
                char[] v = {'o','ó','ô'};
                vet = v;
            } else if (c == 'u'){
                char[] v = {'u','ú','û'};
                vet = v;
            }
            for (int i = 0; i < vet.length; i++) {
                c = vet[i];
                for (int j = 0; j < resposta.length(); j++) {
                    if (resposta.charAt(j) == c){
                        aux[j] = c;
                        tem = true;
                    }
                }
            }
        } else {
            for (int i = 0; i < resposta.length(); i++) {
                if (resposta.charAt(i) == c){
                    aux[i] = c;
                    tem = true;
                }
            }
        }
        return tem;
    }

    public static String printCharVet(char[] vet){
        String saida = "";
        for (int i = 0; i < vet.length; i++) {
            saida += vet[i] + " ";
        }
        return saida;
    }

    public static String converteCharVet(char[] vet){
        String saida = "";
        for (int i = 0; i < vet.length; i++) {
            saida += vet[i];
        }
        return saida;
    }
}
